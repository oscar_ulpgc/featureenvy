package featureenvy;

public class Address {
    
    private final String street;
    private final String city;
    private final String postalCode;

    public Address(String street, String city, String postalCode) {
        this.street = street;
        this.city = city;
        this.postalCode = postalCode;
    }
    
    public String getCompleteAddress() {
        return street + "." + city + "." + postalCode;
    }
}
